Configuration:
	Php: 7.2.12
	Server: Apache/2.4.37 (Xampp)
	DataBase: mysqlnd 5.6 (Xampp)
	IDE: Netbeans 8.2
	

Steps:

with apache and mysql services enabled execute the file CreateDB.php located in the init folder,
that file create a database called "products",  two tables, "product" and "type", and a user. This file also
redirects you to the Index.php file.

When you finish the revision if you want you could delete the database by running the file
DropDB.php located in the init folder.

On top of the index.php files i put some information about some design decisions