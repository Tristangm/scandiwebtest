<?php

/* 
 * 
 */

$host="localhost"; 

$root="root"; 
$root_password=""; 
$db="products"; 

    try {
        $pdo  = new PDO("mysql:host=$host", $root, $root_password);

        $pdo ->exec("DROP DATABASE $db") 
        or die(print_r($pdo ->errorInfo(), true));       
    } catch (PDOException $e) {
        die("DB ERROR: ". $e->getMessage());
    }