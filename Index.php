<!DOCTYPE html>

<!--
On this page I added the option of editing since the structure I had created allowed me to implement it in a "simple" way, 
 I hope it is not a problem that I have developed this extra functionality.

I would also like to comment that it is the first time I work with PHP and a database, in case there is some small error in the structure or syntax.
-->
<html>
    <head>
        <?php
        require_once "./products/Product.php";
        require_once "./types/Type.php";
        echo isset($_POST['action']);

        $products = new Product();
        $productList = $products->getAll();
        ?>
        <meta charset="UTF-8">
        <title>List</title>
        <link href="content/css/bootstrap.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="my-3 my-md-5">
            <div class="container">
                <div class="page-header row">     
                    <div class="col-md-6">
                        <h1>Product List</h1>
                    </div>
                    <div class="col">                        
                        <div class="text-right">
                            <div class="dropdown">
                                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Actions
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="./Edit.php">Add New</a>
                                    <a class="dropdown-item" href="#" onclick="javascript:deleteProducts()">Mass Delete</a>
                                    <a class="dropdown-item" href="#" onclick="javascript:editProduct()">Edit</a>
                                </div>
                            </div>
                        </div>
                    </div>    
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-10 offset-md-1">
                        <div class="row">
                            <?php
                            if (count($productList) <= 0 ) {
                                echo "there are no elements ";
                            } else {
                                foreach ($productList as $row) {
                                    ?>
                                    <div class="col-lg-3  col-md-4 mb-4">
                                        <div class="card">
                                            <div class="card-header">
                                                <input type="checkbox"  name="deleteCbx" value="<?Php echo $row['SKU']; ?>">
                                                <?Php echo $row[$products::SKU_Index]; ?>
                                            </div>
                                            <div class="card-body p-0">
                                                <ul class="list-group list-group-flush">                                        
                                                    <li class="list-group-item">
                                                        Name: <strong> <?Php echo $row[$products::name_Index]; ?> </strong>
                                                    </li>
                                                    <li class="list-group-item">
                                                        Price: <strong><?Php echo $row[$products::price_Index]; ?></strong>
                                                    </li>
                                                    <li class="list-group-item">
                                                        Type: <strong><?Php
                                                            echo $row[$products::tableType_NameIndex];
                                                            ?></strong>
                                                    </li>
                                                    <li class="list-group-item">
                                                        <?Php echo $row[$products::tableType_UnitIndex] . ":"; ?>
                                                        <strong><?Php echo $row['Value']; ?></strong>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                        </div> 
                    </div>
                </div>

            </div>        
        </div>

        <script src="content/js/jQuery-3.3.1.js" type="text/javascript"></script>
        <script src="content/js/popper.min.js" type="text/javascript"></script>
        <script src="content/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="content/js/index/index.js" type="text/javascript"></script>
    </body>
</html>
