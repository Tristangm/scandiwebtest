/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {
    //the call to this method allows to initially show the fields of 
    //the selected type in the edition    
    ChangeForm($("#Type"));
});

//This function changes the display in type form 
//first hide all the forms using a common class 
//for the three and then display the only that have the matched id
function ChangeForm(element) {
    var res = $(element).val();
    console.log(res);
    $(".type_form").hide();
    $("#form_" + res).show();
}

//First use the validation method and if the answer is true, the method will 
//send the serialized data to a service that manages insert/ edit actions.
function save() {
    if (validation()) {
        $.ajax({
            type: "POST",
            url: './services/insert.php',
            dataType: 'json',
            data: $("#EditForm").serialize(),
            success: function (response) {
                console.log(response);
                if (response.status == "ok") {
                    document.location.href="./edit.php"
                    alert("successful creation");
                } else {
                    alert(response.msg);
                    console.log(response.errorMsg);
                }
            }
        });
    } else {
        alert("you must enter all the data");
    }
}

//this validate that all the inputs are filled
function validation() {
    var typeVal = parseInt($("#Type").val());
    if ($("#SKU").val() != "" &&
            $("#Name").val() != "" &&
            $("#Price").val() != "" &&
            typeVal > 0) {
        switch (typeVal) {
            case 1:
                return $("#Size").val() != "";
            case 2:
                return $("#Weight").val() != "";
            case 3:
                return $("#Height").val() != "" && $("#Width").val() != "" && $("#Length").val() != "";
            default:
                return false;
        }

        return true;
    }
    return false;
}
//this is a validation using in the on change event in the numeric type input 
//thisthats control with an regex the value typed by the user 
function numberValidation(element) {
    
    
    var x = $(element).val();
      if(x<1){
        $(element).val("");
    }
}

//this is an alternative for the redirect method used for cleaning the form
function cleanForm() {
    $("#SKU").val("");
    $("#Name").val("");
    $("#Price").val("");
    $("#Type").val("0")
    $("#Size").val("");
    $("#Weight").val("");
    $("#Height").val("");
    $("#Width").val("");
    $("#Length").val("");
    $(".type_form").hide();
}

