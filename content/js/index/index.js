/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//method to redirect to the editing page by passing the SKU as a parameter 
function editProduct() {

    if ($("[name='deleteCbx']:checked").length == 1) {
        document.location.href = "./edit.php?SKU=" + $("[name='deleteCbx']:checked").val();
    } else {
        alert("you must select one product");
    }
}

//method to mass delete products, it also display an alert message on error
function deleteProducts() {
    if ($("[name='deleteCbx']:checked").length > 0) {
        if (confirm("Are you sure? This action would permanently delete the product.")) {
            $.each($("[name='deleteCbx']:checked"), function () {
                $.ajax({
                    type: "POST",
                    url: './services/delete.php',
                    dataType: 'json',
                    data: {"SKU": $(this).val()},
                    success: function (response) {
                        
                    }, 
                    fail:function(response){
                        alert(response.msg);
                    }
                });
            });
            document.location.href = "./index.php";
        }
    }
}
