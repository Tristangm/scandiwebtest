<?php

/*
 * This file allows you to make ajax calls from jQuery 
 * to receive the product data and manage the insert / edit actions
 */

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
require_once "../products/Product.php";

if (!empty($_POST['SKU']) &&
        !empty($_POST['Name']) &&
        !empty($_POST['Price']) &&
        !empty($_POST['Type'])) {
    $sucess = false;
    switch ($_POST['Type']) {
        case 1:
            $sucess = !empty($_POST["Size"]);
            if ($sucess)
                $value = $_POST["Size"];
            break;
        case 2:
            $sucess = !empty($_POST["Weight"]);
            if ($sucess)
                $value = $_POST["Weight"];
            break;
        case 3:
            $sucess = !empty($_POST["Height"]) && !empty($_POST["Width"]) && !empty($_POST["Length"]);
            if ($sucess)
                $value = $_POST["Height"] . "x" . $_POST["Width"] . "x" . $_POST["Length"];
            break;
        default :
            $sucess = false;
            break;
    }
    if ($sucess) {

        $product = new Product();
        $product->SKU = $_POST['SKU'];
        $product->name = $_POST['Name'];
        $product->price = $_POST['Price'];
        $product->type = $_POST['Type'];
        $product->value = $value;
        if ($_POST["Edit"]) {
            $sucess = $product->edit();
        } else {
            $sucess = $product->insert();
        }
        if ($sucess)
            echo json_encode(array("status" => "ok"));
        else
            echo json_encode(array("status" => "nok", "msg" => "it was not possible to complete the process, try again later", "errorMsg" => $sucess));
    } else {
        echo json_encode(array("status" => "nok", "msg" => "process could not be finished, try again later"));
    }
} else {
    echo json_encode(array("status" => "nok", "msg" => "process could not be finished, try again later"));
}
 
