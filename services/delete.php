<?php

/*
 * This file allows you to make ajax calls from jQuery 
 * to receive the SKU data and manage the delete
 */

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
require_once "../products/Product.php";

if (!empty($_POST['SKU'])) {
    $success = false;
    $product = new Product();
    $product->SKU = $_POST['SKU'];
    $success = $product->delete();
    if($success){
         echo json_encode(array("status" => "ok"));
    }else{
         echo json_encode(array("status" => "nok", "msg" => "process could not be finished, try again later "));
    }
    
} else {
    echo json_encode(array("status" => "nok", "msg" => "process could not be finished, try again later"));
}
 
