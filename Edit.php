<!DOCTYPE html>
<!--
 
-->
<html>
    <head>
        <?php
        require_once "./types/Type.php";
        require_once "./products/Product.php";

        $type = new Type();

        $allTypes = $type->getAll();
        
        $product = new Product();
        $edit = isset($_GET["SKU"]);

        if ($edit) {
            $product->SKU = $_GET["SKU"];
            $product->getProduct();
            if ($product->type == 3) {
                $values = explode("x", $product->value);
            }
        }
        ?>
        <meta charset="UTF-8">
        <title>List</title>
        <link href="content/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="content/css/mainStyle.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>

        <div class="my-3 my-md-5">
            <div class="container">
                <div class="page-header">                    
                    <h1><?Php echo $edit ? "Product Edit" : "Product Add"; ?></h1>
                    <div>
                        <div class="text-right">
                            <a href="./index.php" id="Filtrar" class="btn btn-secondary btn-sm btn-filter text-uppercase">
                                Home
                            </a>
                            <a href="#" id="Filtrar" class="btn btn-primary btn-sm btn-filter text-uppercase" onclick="javascript:save()">
                                Save
                            </a>
                        </div>
                    </div>
                </div>
                <hr>
                <form id="EditForm" class="col-10 offset-1">
                    <input class="hidden" value="<?Php echo $edit; ?>" id="Edit" name="Edit" >
                    <div class="card">                    
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label" for="SKU">SKU<sup class="form-required asterisk">*</sup></label>
                                        <input type="text" id="SKU" name="SKU" class="form-control input-sm" value="<?Php echo $product->SKU; ?>" title="Unique SKU" <?Php echo $edit ? "readonly" : ""; ?> />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label" for="Name">Name<sup class="form-required asterisk">*</sup></label>
                                        <input type="text" id="Name" name="Name" class="form-control input-sm"  value="<?Php echo $product->name; ?>" title="name of the product"/>
                                    </div>                            
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label" for="Price">Price<sup class="form-required asterisk">*</sup></label>
                                        <input type="number" id="Price" name="Price" class="form-control input-sm"  value="<?Php echo $product->price; ?>" onchange="javascript:numberValidation(this);" title="numeric price of the product"/>
                                    </div>
                                </div>
                                <div class="col-md-6 ">
                                    <div class="form-group">
                                        <label class="form-label" for="Type">Type<sup class="form-required asterisk">*</sup></label>
                                        <select class="form-control input-sm" id="Type" name="Type" onchange="javascript:ChangeForm(this);" title="type of product">
                                            <option value="0" selected="selected">...</option>
                                            <?Php
                                            foreach ($allTypes as $row) {
                                                ?>
                                                <option value="<?Php echo $row[$type::id_Index]; ?>" <?Php echo $product->type == $row[$type::id_Index] ? "selected" : ""; ?>><?Php echo $row[$type::name_Index]; ?></option>
                                            <?Php } ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-12 type_form hidden" name="DVD_Form" id="form_1">
                                    <div class="form-group">
                                        <label class="form-label" for="Size">Size<sup class="form-required asterisk">*</sup></label>
                                        <input type="number" id="Size" name="Size" class="form-control input-sm" onchange="javascript:numberValidation(this);" value="<?Php echo $product->type == 1 ? $product->value : ""; ?>" title="size in Mb"/>
                                        <span class="text-muted"> Please provide de size in Mb</span>
                                    </div>
                                </div>
                                <div class="col-12 type_form hidden" name="Book_Form" id="form_2">
                                    <div class="form-group">
                                        <label class="form-label" for="Weight">Weight<sup class="form-required asterisk">*</sup></label>
                                        <input type="number" id="Weight" name="Weight" class="form-control input-sm" onchange="javascript:numberValidation(this);" value="<?Php echo $product->type == 2 ? $product->value : "";?>" title="weight in gr"/>
                                        <span class="text-muted"> Please provide de weight in gr</span>
                                    </div>
                                </div>
                                <div class="col-12 type_form hidden" name="Furniture_Form" id="form_3">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="form-label" for="Height">Height<sup class="form-required asterisk">*</sup></label>
                                                <input type="number" id="Height" name="Height" class="form-control input-sm" onchange="javascript:numberValidation(this);" value="<?Php echo $product->type == 3 ? $values[0] : ""; ?>" title="height in cm"/>
                                                <span class="text-muted"> Please provide de height in cm</span>
                                            </div>                              
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="form-label" for="Width">Width<sup class="form-required asterisk">*</sup></label>
                                                <input type="number" id="Width" name="Width" class="form-control input-sm" onchange="javascript:numberValidation(this);" value="<?Php echo $product->type == 3 ? $values[1] : ""; ?>" title="width in cm"/>
                                                <span class="text-muted"> Please provide de width in cm</span>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="form-label" for="Length">Length<sup class="form-required asterisk">*</sup></label>
                                                <input type="number" id="Length" name="Length" class="form-control input-sm" onchange="javascript:numberValidation(this);" value="<?Php echo $product->type == 3 ? $values[2] : ""; ?>" title="length in cm"/>
                                                <span class="text-muted"> Please provide de length in cm</span>
                                            </div>   
                                        </div>             
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>        
        </div>

        <script src="content/js/jQuery-3.3.1.js" type="text/javascript"></script>
        <script src="content/js/popper.min.js" type="text/javascript"></script>
        <script src="content/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="content/js/edit/Edit.js" type="text/javascript"></script>
    </body>
</html>
