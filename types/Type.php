<?php

include_once __DIR__ . "/../utils/BBDD.php";

/**
 *
 * @author tristan
 */
class Type {

     //this are the properties of the type class
    var $id;
    var $name;
    var $unit;

     //these are the constants of the type class I use to build the 
    //database queries, they contain the names of the fields in the database 
    //as well as the names of the table
    const id_Index = "ID_Type";
    const name_Index = "Name_Type";
    const unit_Index = "Unit";
    const tableName_Type = "type";

    //This function get all the types
    function getAll() {
        try {
            $pdo = new PDO('mysql:host=' . BBDD::$host . ';dbname=' . BBDD::$dbName, BBDD::$user, BBDD::$pass);
            $statement = $pdo->query("SELECT * FROM `" . $this::tableName_Type . "`");
            $row = $statement->fetchAll(PDO::FETCH_ASSOC);
            $pdo = null;
            return $row;
        } catch (PDOException $ex) {
            return false;
        }
    }

}
