<?php

include_once __DIR__ . "/../utils/BBDD.php";



/**
 * Description of Product
 *
 * @author tristan
 */
class Product {

    //this are the properties of the product class
    var $SKU;
    var $name;
    var $price;
    var $type;
    var $value;

    //these are the constants of the product class I use to build the 
    //database queries, they contain the names of the fields in the database 
    //as well as the names of the tables
    const SKU_Index = "SKU";
    const name_Index = "Name";
    const price_Index = "Price";
    const type_Index = "Type";
    const value_Value = "Value";
    const tableName_Product = "product";
    const tableName_Type = "type";
    const tableType_NameIndex = "Name_Type";
    const tableType_UnitIndex = "Unit";

    
    function insert() {
        try {
            $pdo = new PDO('mysql:host=' . BBDD::$host . ';dbname=' . BBDD::$dbName, BBDD::$user, BBDD::$pass);
            $statement = $pdo->prepare("INSERT INTO `" . $this::tableName_Product . "`(`" . $this::SKU_Index . "`, `" . $this::name_Index . "`, `" . $this::price_Index . "`, `" . $this::type_Index . "`, `" . $this::value_Value . "`) VALUES (?,?,?,?,?)");
            $res = $statement->execute(array($this->SKU, $this->name, $this->price, $this->type, $this->value));
            $pdo = null;
            return $res;
        } catch (PDOException $ex) {
            return $ex->getMessage();
        }
    }

    //This function get all the productas and also get their type information
    function getAll() {
        try {
            $pdo = new PDO('mysql:host=' . BBDD::$host . ';dbname=' . BBDD::$dbName, BBDD::$user, BBDD::$pass);
            $statement = $pdo->query("SELECT * FROM " . $this::tableName_Product . " A, " . $this::tableName_Type . " B Where A.Type = B.ID_Type");
            $row = $statement->fetchAll(PDO::FETCH_ASSOC);
            $pdo = null;
            return $row;
        } catch (PDOException $ex) {
            return TRUE;
        }
    }

    function delete() {
        try {
            $pdo = new PDO('mysql:host=' . BBDD::$host . ';dbname=' . BBDD::$dbName, BBDD::$user, BBDD::$pass);
            $statement = $pdo->prepare("DELETE FROM " . $this::tableName_Product . " WHERE " . $this::SKU_Index . " = ? ");
            $res = $statement->execute(array($this->SKU));
            $pdo = null;
            return $res;
        } catch (PDOException $ex) {
            return $ex->getMessage();
        }
    }

    //This function get only one product based on its unique SKU field
    function getProduct() {
        $pdo = new PDO('mysql:host=' . BBDD::$host . ';dbname=' . BBDD::$dbName, BBDD::$user, BBDD::$pass);
        $statement = $pdo->prepare("SELECT * FROM " . $this::tableName_Product . "  Where " . $this::SKU_Index . "= ?");
        $statement->execute(array($this->SKU));
        $row = $statement->fetch(PDO::FETCH_ASSOC);
        $pdo = null;
        $this->SKU = $row[$this::SKU_Index];
        $this->name = $row[$this::name_Index];
        $this->price = $row[$this::price_Index];
        $this->type = $row[$this::type_Index];
        $this->value = $row[$this::value_Value];

        return $row;
    }

    function edit() {
        try {
            $pdo = new PDO('mysql:host=' . BBDD::$host . ';dbname=' . BBDD::$dbName, BBDD::$user, BBDD::$pass);
            $statement = $pdo->prepare("UPDATE `" . $this::tableName_Product . "` SET `" . $this::name_Index . "`=?,`" . $this::price_Index . "`=?,`" . $this::type_Index . "`=?,`" . $this::value_Value . "`=? WHERE `" . $this::SKU_Index . "` = ?");
            $res = $statement->execute(array($this->name, $this->price, $this->type, $this->value, $this->SKU));

            return $res;
        } catch (PDOException $ex) {
            return $ex->getMessage();
        }
    }

}
